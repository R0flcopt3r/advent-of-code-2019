#! /usr/bin/python3


input = open("./input")
program = [] 
for item in input.read().split(","):
    program.append(int(item));

def decode(program):
    for i in range(0, len(program), 4):
        if program[i] == 1:
            program[program[i+3]] = program[program[i+1]] + program[program[i+2]]
        elif program[i] == 2:
            program[program[i+3]] = program[program[i+1]] * program[program[i+2]]
        elif program[i] == 99: break

    return program[0]


result = 19690720

for noun in range(1, 99):
    for verb in range(1, 99):

        program[1] = int(noun);
        program[2] = int(verb);
        if decode(program.copy()) == result:
            print("{:02d}{:02d}".format(noun,verb))
