#! /usr/bin/python3

input = open("./input")
total_fuel=0

def calc_fuel( mass ):
    base_fuel = mass//3-2
    if base_fuel > 0:
        base_fuel += calc_fuel(base_fuel)
    if base_fuel < 0: return 0
    return base_fuel;

for line in input:
    total_fuel += calc_fuel(int(line))

print(total_fuel)
