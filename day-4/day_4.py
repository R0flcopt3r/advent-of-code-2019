#!/usr/bin/env python3

def gen_num_arr(num):
    digits = []
    while num !=  0:
        digits.append(num%10)
        num = num//10
    digits.reverse()
    return digits

def pass_check(digits):
    pair = 0
    for i in range(0, len(digits)-1):
        if digits[i] < digits[i+1]:
            continue
        elif digits[i] == digits[i+1]:
            pair += 1
        else:
            return False
    return pair >= 1

if __name__ == '__main__':
    no_passed = 0
    for x in range(138241,674034):
        if pass_check(gen_num_arr(x)):
            no_passed += 1
    print(no_passed)
